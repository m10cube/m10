M10SE02

<p align="center"><img src="/ELECTRICAL/IMAGES/M10SE02-01.jpg"></p>

Development and fabrication of M10SE01-01 stopped in favor of M10SE02-01
Some serious issues discovered during building in Breadboard an experimental version of M10SE01-01 circuit and that was:

UART0:RXD and UART0:TXD pins can not be used and better remain only for programming the ESP32 MINIKIT.
Having one UART less we decided to remove RS485 communication.
 
The RS485 idea was to connect other M10SE01 modules distant apart for sensing the near by environment. Since that is not possible any more an I2C solution  was adapted using the ideas taken from a Sparkfun design.
 
The 2 RJ45 connectors remaining in hope to daisy chain from one board to the other the 2 differential I2C signals. Board sent for fabrication

SPECS:

Ambient Light, UV Light, VOC, Digital Sound (MEMS)

Temperature, Humidity, Barometric

ESP32 minikit, GPS, LoRa, I2C EXTENDER (2XRJ45 FOR DAISY CHAIN)


M10SE02-01 PCBs arrived and bulid.
The board is working feeding enviromental data in influx database and grafana as GUI

There are some PCB errors and M10SE02-02 started but not finished yet

Soon...

A M10CUBE sensor board software:
That is written in Arduino IDE using all on board hardware running on the embedded ESP32 module.


<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br> 





