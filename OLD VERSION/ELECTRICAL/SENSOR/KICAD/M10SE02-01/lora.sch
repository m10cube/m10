EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 8
Title "M10SE02"
Date "2020-10-18"
Rev ""
Comp "M10CUBE"
Comment1 ""
Comment2 "GPS, LoRa, I2C EXTENDER"
Comment3 "Temperature, Humidity, Barometric"
Comment4 "Ambient Light, UV Light, VOC, Sound"
$EndDescr
Text Notes 3300 1400 0    50   ~ 0
ESP8266 FEATHER PIN\nint PIN_LORA_NSS    0  // 5th pin on lowerside\nint PIN_LORA_RESET  17 // Unused, connect to NRST on the board to RST \nint PIN_LORA_DIO_1  15 // IRQ pin 6th pin on lowerside\nint PIN_LORA_BUSY   16 // 4th pin on lowerside
Wire Wire Line
	1300 4550 1300 4600
Wire Wire Line
	1300 4600 1300 4700
Wire Wire Line
	800  2300 800  4600
Wire Wire Line
	800  4600 1300 4600
Connection ~ 1300 4600
Wire Wire Line
	2250 4900 2300 4900
Wire Wire Line
	1300 4250 1300 4200
Text Label 1300 4250 0    10   ~ 0
3.3V
Wire Wire Line
	1300 5100 1300 5200
$Comp
L Transistor_BJT:BC818 Q3
U 1 1 F1614351
P 1400 4900
F 0 "Q3" H 1300 5000 42  0000 L BNN
F 1 "S9013" H 1550 5000 42  0000 L BNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 1400 4900 50  0001 C CNN
F 3 "" H 1400 4900 50  0001 C CNN
	1    1400 4900
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 5F2ADE55
P 2100 4900
F 0 "R10" H 2050 4950 42  0000 L BNN
F 1 "10K" H 2050 4800 42  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2100 4900 50  0001 C CNN
F 3 "" H 2100 4900 50  0001 C CNN
	1    2100 4900
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 4C88E097
P 1300 4400
F 0 "R8" H 1250 4450 42  0000 L BNN
F 1 "10K" H 1250 4300 42  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1300 4400 50  0001 C CNN
F 3 "" H 1300 4400 50  0001 C CNN
	1    1300 4400
	1    0    0    -1  
$EndComp
Text Notes 8575 1275 0    85   ~ 0
External SMA antenna
Text Notes 1000 3700 0    85   ~ 0
RF antenna control
$Comp
L LORA_EBYTE:E22-900M30S IC?
U 1 1 5F1F071D
P 4250 3000
AR Path="/5F1F071D" Ref="IC?"  Part="1" 
AR Path="/5F139938/5F1F071D" Ref="IC1"  Part="1" 
F 0 "IC1" H 4450 3700 42  0000 L BNN
F 1 "E22-900M30S" H 3750 3700 42  0000 L BNN
F 2 "LORA_EBYTE:E22-900M30S" H 4250 3000 50  0001 C CNN
F 3 "" H 4250 3000 50  0001 C CNN
	1    4250 3000
	1    0    0    -1  
$EndComp
Text Notes 3250 2050 0    85   ~ 0
E22-900M30S module
Wire Wire Line
	3550 3600 3750 3600
Wire Wire Line
	3750 3500 3550 3500
Wire Wire Line
	3550 3400 3750 3400
Wire Wire Line
	3550 3300 3750 3300
Wire Wire Line
	3550 3200 3750 3200
Wire Wire Line
	3550 3100 3750 3100
Wire Wire Line
	3750 3000 3550 3000
Wire Wire Line
	3550 2900 3750 2900
Wire Wire Line
	3750 2800 3550 2800
Wire Wire Line
	4750 2800 4750 2900
Wire Wire Line
	4750 2900 4750 3000
Connection ~ 4750 2900
Wire Wire Line
	4750 3000 4750 3100
Connection ~ 4750 3000
Wire Wire Line
	4750 3100 4750 3200
Connection ~ 4750 3100
Wire Wire Line
	4750 3200 4750 3400
Connection ~ 4750 3200
Wire Wire Line
	4750 3400 4750 3500
Connection ~ 4750 3400
Wire Wire Line
	4750 3500 4750 3600
Connection ~ 4750 3500
Wire Wire Line
	4750 3600 4750 3700
Connection ~ 4750 3600
Wire Wire Line
	4750 3700 4750 3950
Connection ~ 4750 3700
$Comp
L power:GND #PWR015
U 1 1 5F24FCA5
P 1300 5200
F 0 "#PWR015" H 1300 4950 50  0001 C CNN
F 1 "GND" H 1305 5027 50  0000 C CNN
F 2 "" H 1300 5200 50  0001 C CNN
F 3 "" H 1300 5200 50  0001 C CNN
	1    1300 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 2500 3600 2600
Wire Wire Line
	3600 2600 3750 2600
Wire Wire Line
	3800 2600 3750 2600
Wire Wire Line
	3750 2600 3750 2700
Connection ~ 3750 2600
$Comp
L LORA_EBYTE:E22-900M22S IC?
U 1 1 5F26204F
P 6850 3000
AR Path="/5F26204F" Ref="IC?"  Part="1" 
AR Path="/5F139938/5F26204F" Ref="IC2"  Part="1" 
F 0 "IC2" H 7050 3700 42  0000 L BNN
F 1 "E22-900M22S" H 6350 3700 42  0000 L BNN
F 2 "LORA_EBYTE:E22-900M22S" H 6850 3000 50  0001 C CNN
F 3 "" H 6850 3000 50  0001 C CNN
	1    6850 3000
	1    0    0    -1  
$EndComp
Text Notes 5850 2050 0    85   ~ 0
E22-900M22S module
Wire Wire Line
	6150 3600 6350 3600
Wire Wire Line
	6350 3500 6150 3500
Wire Wire Line
	6150 3400 6350 3400
Wire Wire Line
	6150 3300 6350 3300
Wire Wire Line
	6150 3200 6350 3200
Wire Wire Line
	6150 3100 6350 3100
Wire Wire Line
	6350 3000 6150 3000
Wire Wire Line
	6150 2900 6350 2900
Wire Wire Line
	6350 2800 6150 2800
Wire Wire Line
	7350 2800 7350 2900
Wire Wire Line
	7350 2900 7350 3000
Connection ~ 7350 2900
Wire Wire Line
	7350 3000 7350 3100
Connection ~ 7350 3000
Wire Wire Line
	7350 3100 7350 3200
Connection ~ 7350 3100
Wire Wire Line
	7350 3200 7350 3300
Connection ~ 7350 3200
Wire Wire Line
	7350 3400 7350 3500
Connection ~ 7350 3400
Wire Wire Line
	7350 3500 7350 3600
Connection ~ 7350 3500
Wire Wire Line
	7350 3600 7350 3700
Connection ~ 7350 3600
Wire Wire Line
	7350 3700 7350 3950
Connection ~ 7350 3700
Wire Wire Line
	6200 2500 6200 2600
Wire Wire Line
	6200 2600 6350 2600
Connection ~ 7350 3300
Wire Wire Line
	7350 3300 7350 3400
$Comp
L power:+3.3V #PWR017
U 1 1 5F2481A6
P 3600 2500
F 0 "#PWR017" H 3600 2350 50  0001 C CNN
F 1 "+3.3V" H 3615 2673 50  0000 C CNN
F 2 "" H 3600 2500 50  0001 C CNN
F 3 "" H 3600 2500 50  0001 C CNN
	1    3600 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR014
U 1 1 5F249781
P 1300 4200
F 0 "#PWR014" H 1300 4050 50  0001 C CNN
F 1 "+3.3V" H 1315 4373 50  0000 C CNN
F 2 "" H 1300 4200 50  0001 C CNN
F 3 "" H 1300 4200 50  0001 C CNN
	1    1300 4200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR019
U 1 1 5F24DC9E
P 6200 2500
F 0 "#PWR019" H 6200 2350 50  0001 C CNN
F 1 "+3.3V" H 6215 2673 50  0000 C CNN
F 2 "" H 6200 2500 50  0001 C CNN
F 3 "" H 6200 2500 50  0001 C CNN
	1    6200 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5F2521F8
P 9275 1975
F 0 "#PWR021" H 9275 1725 50  0001 C CNN
F 1 "GND" H 9280 1802 50  0000 C CNN
F 2 "" H 9275 1975 50  0001 C CNN
F 3 "" H 9275 1975 50  0001 C CNN
	1    9275 1975
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5F25CC41
P 2850 2750
F 0 "C5" H 2965 2796 50  0000 L CNN
F 1 "10uF" H 2965 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_Elec_4x5.8" H 2888 2600 50  0001 C CNN
F 3 "~" H 2850 2750 50  0001 C CNN
	1    2850 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 2600 2850 2600
Connection ~ 3600 2600
$Comp
L Device:C C4
U 1 1 5F25FE6D
P 2450 2750
F 0 "C4" H 2565 2796 50  0000 L CNN
F 1 "100nF" H 2565 2705 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2488 2600 50  0001 C CNN
F 3 "~" H 2450 2750 50  0001 C CNN
	1    2450 2750
	1    0    0    -1  
$EndComp
Connection ~ 2850 2600
Wire Wire Line
	2450 2600 2850 2600
Wire Wire Line
	2450 2900 2850 2900
$Comp
L power:GND #PWR016
U 1 1 5F266227
P 2450 2900
F 0 "#PWR016" H 2450 2650 50  0001 C CNN
F 1 "GND" H 2455 2727 50  0000 C CNN
F 2 "" H 2450 2900 50  0001 C CNN
F 3 "" H 2450 2900 50  0001 C CNN
	1    2450 2900
	1    0    0    -1  
$EndComp
Connection ~ 2450 2900
Text HLabel 3550 2900 0    50   Input ~ 0
SCK_ESP
Text HLabel 3550 3200 0    50   Input ~ 0
RST_ESP
Text HLabel 3550 3300 0    50   Input ~ 0
BUSY_LORA
Text HLabel 3550 3400 0    50   Input ~ 0
RXEN_LORA
Text HLabel 3550 3500 0    50   Input ~ 0
TXEN_LORA
Text HLabel 3550 2800 0    50   Input ~ 0
SS_LORA
Text HLabel 3550 3000 0    50   Input ~ 0
MOSI_ESP
Text HLabel 3550 3100 0    50   Input ~ 0
MISO_ESP
Text HLabel 3550 3600 0    50   Input ~ 0
DIO1_LORA
Text Label 3125 3700 0    50   ~ 0
DIO2
Wire Wire Line
	3125 3700 3750 3700
Text Label 2300 1850 3    50   ~ 0
DIO2
Text HLabel 900  2400 1    50   Input ~ 0
TXEN_LORA
Text HLabel 800  2300 1    50   Input ~ 0
RXEN_LORA
Text Label 7350 2600 0    50   ~ 0
ANT_LORA
Text Label 4750 2600 0    50   ~ 0
ANT_LORA
Text HLabel 6150 2900 0    50   Input ~ 0
SCK_ESP
Text HLabel 6150 3200 0    50   Input ~ 0
RST_ESP
Text HLabel 6150 3300 0    50   Input ~ 0
BUSY_LORA
Text HLabel 6150 3400 0    50   Input ~ 0
RXEN_LORA
Text HLabel 6150 3500 0    50   Input ~ 0
TXEN_LORA
Text HLabel 6150 2800 0    50   Input ~ 0
SS_LORA
Text HLabel 6150 3000 0    50   Input ~ 0
MOSI_ESP
Text HLabel 6150 3100 0    50   Input ~ 0
MISO_ESP
Text HLabel 6150 3600 0    50   Input ~ 0
DIO1_LORA
Text Label 5725 3700 0    50   ~ 0
DIO2
Wire Wire Line
	6350 3700 5725 3700
Wire Wire Line
	2300 1850 2300 3375
$Comp
L Device:R R9
U 1 1 5F58D3F0
P 1400 3375
F 0 "R9" V 1193 3375 50  0000 C CNN
F 1 "0" V 1284 3375 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1330 3375 50  0001 C CNN
F 3 "~" H 1400 3375 50  0001 C CNN
	1    1400 3375
	0    1    1    0   
$EndComp
Wire Wire Line
	1550 3375 2300 3375
Connection ~ 2300 3375
Wire Wire Line
	2300 3375 2300 4900
Wire Wire Line
	900  2400 900  3375
Wire Wire Line
	900  3375 1250 3375
Text Notes 7600 3375 0    50   ~ 0
DESIGN NOTE:\nDIO1, DIO2 are GPIO , configurable for multiple\nfunctions ; DIO2 can connect to TXEN , without\nconnecting to IO of MCU , for controlling RF\nswitch TX, Please see more in SX126x\ndatasheet, can be floated when free.
Text Label 8975 1675 2    50   ~ 0
ANT_LORA
Wire Wire Line
	1600 4900 1950 4900
Text Notes 7575 3975 0    50   ~ 0
DESIGN NOTE:\nZero Ohm resistors can directly connect LoRa modules\nto the RSPI 40 pin connector without the need of the ESP32 module
$Comp
L ANTENNA:SMA_EDGE J?
U 1 1 5F5064AB
P 9175 1675
AR Path="/5F5064AB" Ref="J?"  Part="1" 
AR Path="/5F139938/5F5064AB" Ref="J3"  Part="1" 
F 0 "J3" H 9225 1942 50  0000 C CNN
F 1 "SMA_EDGE" H 9225 1851 50  0000 C CNN
F 2 "Connector_Coaxial:SMA_Samtec_SMA-J-P-X-ST-EM1_EdgeMount" H 9175 1675 50  0001 C CNN
F 3 "https://gr.mouser.com/datasheet/2/975/1498648656DS-CON-EDGE-SMA-4-1589063.pdf" H 9175 1675 50  0001 C CNN
	1    9175 1675
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9275 1975 9275 1875
Connection ~ 9275 1975
$Comp
L power:GND #PWR0101
U 1 1 5F98AA44
P 7350 3950
F 0 "#PWR0101" H 7350 3700 50  0001 C CNN
F 1 "GND" H 7355 3777 50  0000 C CNN
F 2 "" H 7350 3950 50  0001 C CNN
F 3 "" H 7350 3950 50  0001 C CNN
	1    7350 3950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5F98AECB
P 4750 3950
F 0 "#PWR0104" H 4750 3700 50  0001 C CNN
F 1 "GND" H 4755 3777 50  0000 C CNN
F 2 "" H 4750 3950 50  0001 C CNN
F 3 "" H 4750 3950 50  0001 C CNN
	1    4750 3950
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5F98CAC6
P 9700 1875
F 0 "#FLG0101" H 9700 1950 50  0001 C CNN
F 1 "PWR_FLAG" H 9700 2048 50  0000 C CNN
F 2 "" H 9700 1875 50  0001 C CNN
F 3 "~" H 9700 1875 50  0001 C CNN
	1    9700 1875
	1    0    0    -1  
$EndComp
Wire Wire Line
	9275 1875 9700 1875
Connection ~ 9275 1875
$EndSCHEMATC
