EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Module:WeMos_D1_mini U3
U 1 1 5F1259A4
P 1700 1750
AR Path="/5F0B8C7C/5F1259A4" Ref="U3"  Part="1" 
AR Path="/5F0E5853/5F1259A4" Ref="U?"  Part="1" 
AR Path="/5F139938/5F1259A4" Ref="U?"  Part="1" 
F 0 "U3" H 1500 900 50  0000 C CNN
F 1 "WeMos_D1_mini" H 2100 900 50  0000 C CNN
F 2 "WEMOS_D1_MINI:wemos-d1-mini-with-pin-header" H 1700 600 50  0001 C CNN
F 3 "https://wiki.wemos.cc/products:d1:d1_mini#documentation" H -150 600 50  0001 C CNN
	1    1700 1750
	1    0    0    -1  
$EndComp
Text GLabel 2100 1850 2    50   Input ~ 0
SCK_ESP
Text GLabel 2100 1950 2    50   Input ~ 0
MISO_ESP
Text GLabel 2100 2050 2    50   Input ~ 0
MOSI_ESP
Text GLabel 2100 1450 2    50   Input ~ 0
SCL_ESP
Text GLabel 2100 1550 2    50   Input ~ 0
SDA_ESP
$Comp
L power:+5V #PWR020
U 1 1 5F1259A0
P 1600 950
AR Path="/5F0B8C7C/5F1259A0" Ref="#PWR020"  Part="1" 
AR Path="/5F0E5853/5F1259A0" Ref="#PWR?"  Part="1" 
AR Path="/5F139938/5F1259A0" Ref="#PWR?"  Part="1" 
F 0 "#PWR020" H 1600 800 50  0001 C CNN
F 1 "+5V" H 1615 1123 50  0000 C CNN
F 2 "" H 1600 950 50  0001 C CNN
F 3 "" H 1600 950 50  0001 C CNN
	1    1600 950 
	1    0    0    -1  
$EndComp
Text Notes 2200 800  0    59   ~ 0
Short traces if board is powered from Wemos. 
$Comp
L Device:Buzzer BZ1
U 1 1 5F12599F
P 4250 1400
AR Path="/5F0B8C7C/5F12599F" Ref="BZ1"  Part="1" 
AR Path="/5F0E5853/5F12599F" Ref="BZ?"  Part="1" 
AR Path="/5F139938/5F12599F" Ref="BZ?"  Part="1" 
F 0 "BZ1" H 4402 1429 50  0000 L CNN
F 1 "Buzzer" H 4402 1338 50  0000 L CNN
F 2 "Buzzer_Beeper:Buzzer_12x9.5RM7.6" V 4225 1500 50  0001 C CNN
F 3 "~" V 4225 1500 50  0001 C CNN
	1    4250 1400
	1    0    0    -1  
$EndComp
Text GLabel 2100 1250 2    50   Input ~ 0
A0
Text GLabel 3550 1700 0    50   Input ~ 0
BUZZER
$Comp
L Device:R R11
U 1 1 5F19B084
P 3700 1700
AR Path="/5F0B8C7C/5F19B084" Ref="R11"  Part="1" 
AR Path="/5F0E5853/5F19B084" Ref="R?"  Part="1" 
AR Path="/5F139938/5F19B084" Ref="R?"  Part="1" 
F 0 "R11" V 3493 1700 50  0000 C CNN
F 1 "R470" V 3584 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3630 1700 50  0001 C CNN
F 3 "~" H 3700 1700 50  0001 C CNN
	1    3700 1700
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR021
U 1 1 5F1A3024
P 4150 1300
AR Path="/5F0B8C7C/5F1A3024" Ref="#PWR021"  Part="1" 
AR Path="/5F0E5853/5F1A3024" Ref="#PWR?"  Part="1" 
AR Path="/5F139938/5F1A3024" Ref="#PWR?"  Part="1" 
F 0 "#PWR021" H 4150 1150 50  0001 C CNN
F 1 "+5V" H 4165 1473 50  0000 C CNN
F 2 "" H 4150 1300 50  0001 C CNN
F 3 "" H 4150 1300 50  0001 C CNN
	1    4150 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #GND01
U 1 1 5F1AACC1
P 1700 2550
AR Path="/5F0B8C7C/5F1AACC1" Ref="#GND01"  Part="1" 
AR Path="/5F0E5853/5F1AACC1" Ref="#GND?"  Part="1" 
AR Path="/5F139938/5F1AACC1" Ref="#GND?"  Part="1" 
F 0 "#GND01" H 1700 2550 50  0001 C CNN
F 1 "GND" H 1700 2429 59  0000 C CNN
F 2 "" H 1700 2550 50  0001 C CNN
F 3 "" H 1700 2550 50  0001 C CNN
	1    1700 2550
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:PN2222A Q3
U 1 1 5F2084BD
P 4050 1700
AR Path="/5F0B8C7C/5F2084BD" Ref="Q3"  Part="1" 
AR Path="/5F0E5853/5F2084BD" Ref="Q?"  Part="1" 
AR Path="/5F139938/5F2084BD" Ref="Q?"  Part="1" 
F 0 "Q3" H 4240 1746 50  0000 L CNN
F 1 "PN2222A" H 4240 1655 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4250 1625 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/PN/PN2222A.pdf" H 4050 1700 50  0001 L CNN
	1    4050 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8675 2350 7875 2350
Text GLabel 7875 2350 0    70   BiDi ~ 0
GND
Wire Wire Line
	8675 1950 7875 1950
Text Label 7875 1950 0    70   ~ 0
SS_SD
Wire Wire Line
	8675 2150 8425 2150
Wire Wire Line
	8425 2550 8425 2150
Connection ~ 8425 2150
Wire Wire Line
	8675 2050 7875 2050
Text Label 7875 2050 0    70   ~ 0
MOSI_SD
Wire Wire Line
	8675 2250 7875 2250
Text Label 7875 2250 0    70   ~ 0
SCK_SD
Wire Wire Line
	8675 2450 7875 2450
Text Label 7875 2450 0    70   ~ 0
MISO_SD
$Comp
L Connector:Micro_SD_Card J?
U 1 1 5F4F9597
P 9575 2150
AR Path="/5F4F9597" Ref="J?"  Part="1" 
AR Path="/5EDCDBA8/5F4F9597" Ref="J?"  Part="1" 
AR Path="/5EFF2438/5F4F9597" Ref="J?"  Part="1" 
AR Path="/5F0B8C7C/5F4F9597" Ref="J13"  Part="1" 
AR Path="/5F0E5853/5F4F9597" Ref="J?"  Part="1" 
AR Path="/5F139938/5F4F9597" Ref="J?"  Part="1" 
F 0 "J13" H 9025 2650 42  0000 L BNN
F 1 "MICROSD-9P" H 9275 2650 42  0000 L BNN
F 2 "SD:micro_sd" H 9575 2150 50  0001 C CNN
F 3 "" H 9575 2150 50  0001 C CNN
	1    9575 2150
	1    0    0    -1  
$EndComp
Text Notes 8150 1375 0    50   ~ 0
SD can be connected on ESP8266 OR on STM32 jumper selected\n\nWEMOS D1 Micro SD. The shield uses SPI bus pins:\nSD pin 5 CK, CLK, SCLK to pin5 / GPIO14\nSD pin 7 DO, DAT0, MISO to pin 6 / GPIO12\nSD pin 3 DI, CMD, MOSI to pin 7 / GPIO13\nSD pin 2 CD, DAT3, SS to pin 8 / GPIO15 recommended\nSD pin 4 VCC, VDD to 3V3 supply\nSD pin 6 VSS, GND to common ground\n
Text GLabel 7875 1950 0    50   Input ~ 0
SS_SD
Text GLabel 7875 2050 0    50   Input ~ 0
MOSI_ESP
Text GLabel 7875 2250 0    50   Input ~ 0
SCK_ESP
Text GLabel 7875 2450 0    50   Input ~ 0
MISO_ESP
$Comp
L power:GND #GND02
U 1 1 5F4F95A8
P 8425 3000
AR Path="/5F0B8C7C/5F4F95A8" Ref="#GND02"  Part="1" 
AR Path="/5F0E5853/5F4F95A8" Ref="#GND?"  Part="1" 
AR Path="/5F139938/5F4F95A8" Ref="#GND?"  Part="1" 
F 0 "#GND02" H 8425 3000 50  0001 C CNN
F 1 "GND" H 8425 2879 59  0000 C CNN
F 2 "" H 8425 3000 50  0001 C CNN
F 3 "" H 8425 3000 50  0001 C CNN
	1    8425 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR023
U 1 1 5F66DE77
P 10375 2750
AR Path="/5F0B8C7C/5F66DE77" Ref="#PWR023"  Part="1" 
AR Path="/5F139938/5F66DE77" Ref="#PWR?"  Part="1" 
F 0 "#PWR023" H 10375 2500 50  0001 C CNN
F 1 "GND" H 10380 2577 50  0000 C CNN
F 2 "" H 10375 2750 50  0001 C CNN
F 3 "" H 10375 2750 50  0001 C CNN
	1    10375 2750
	1    0    0    -1  
$EndComp
Text GLabel 2100 1650 2    50   BiDi ~ 0
R0-RS485
Text GLabel 2100 1750 2    50   BiDi ~ 0
D1-RS485
Text Notes 4850 2800 0    50   ~ 0
WEMOS D1 PINS that is fixed are:\nRX - Connected to RO RS485 IC\nTX - Connected to DI RS485 IC\nD0 - Connected to RE RS485 IC\nSCL - Connected to local I2C\nSDA - Connected to local I2C\n\n\n\nWEMOS D1 PINS that can be interchanged according to board function:\nA0, D3, D4, D5, D6, D7, D8, RX, TX\n\nSENSORS SENARIO 1 (MASTER BOARD)\n- I2C_1 (2 pins) for RASPI\n- I2C_2 (2 pins) for interboard bus\n- SPI (3 pins) for SD\n- GPS (UART) (2 pins)\n-  SPI  (3 pins) for LORA. This is common with SD\n- SS (2 pins) for SD and LoRa\n- PWM (1 pin) for BUZZER \n\nSENSORS SENARIO 2\n- RS485\n- I2C\n - GPS (SPI)\n- LORA (SPI)\n- PM (UART)
Text GLabel 2100 1350 2    50   Input ~ 0
RE_RS485
Text GLabel 2100 2150 2    50   Input ~ 0
SS_ESP
Text GLabel 1300 1650 0    50   BiDi ~ 0
RX_ESP
Text GLabel 1300 1750 0    50   BiDi ~ 0
TX_ESP
Text GLabel 1300 1350 0    50   Input ~ 0
RST_ESP
Text GLabel 4550 5500 2    50   Input ~ 0
BUZZER
Text GLabel 4050 5500 0    50   Input ~ 0
A0
Text GLabel 4550 4250 2    50   BiDi ~ 0
TX_RAS
Text GLabel 4550 4800 2    50   BiDi ~ 0
RX_RAS
Text GLabel 4050 4800 0    50   BiDi ~ 0
TX_ESP8266
Text GLabel 4050 4250 0    50   BiDi ~ 0
RX_ESP8266
Text GLabel 4550 5600 2    50   Input ~ 0
PRE
Text GLabel 2700 5650 2    50   Input ~ 0
SS_SD
Text GLabel 2200 5550 0    50   Input ~ 0
SS_ESP
Text GLabel 2700 5750 2    50   Input ~ 0
SS_LORA
Text GLabel 2700 4250 2    50   BiDi ~ 0
SCL_RAS
Text GLabel 2700 4350 2    50   Input ~ 0
SCL_LOC
Text GLabel 4550 5700 2    50   BiDi ~ 0
DS1820
Text GLabel 4550 4900 2    50   BiDi ~ 0
RXD-PM
Text GLabel 2200 4250 0    50   BiDi ~ 0
SCL_ESP
Text GLabel 4550 5100 2    50   Input ~ 0
BUSY_LORA
Text GLabel 4550 4550 2    50   Input ~ 0
DIO1_LORA
$Comp
L Connector_Generic:Conn_02x02_Row_Letter_Last J14
U 1 1 5F278974
P 2400 4250
F 0 "J14" H 2450 4567 50  0000 C CNN
F 1 "Conn_02x03_Row_Letter_Last" H 2450 4476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 2400 4250 50  0001 C CNN
F 3 "~" H 2400 4250 50  0001 C CNN
	1    2400 4250
	1    0    0    -1  
$EndComp
Text GLabel 2700 4900 2    50   BiDi ~ 0
SDA_RAS
Text GLabel 2700 5000 2    50   Input ~ 0
SDA_LOC
Text GLabel 2200 4900 0    50   BiDi ~ 0
SDA_ESP
$Comp
L Connector_Generic:Conn_02x02_Row_Letter_Last J16
U 1 1 5F27897D
P 2400 4900
F 0 "J16" H 2450 5217 50  0000 C CNN
F 1 "Conn_02x03_Row_Letter_Last" H 2450 5126 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 2400 4900 50  0001 C CNN
F 3 "~" H 2400 4900 50  0001 C CNN
	1    2400 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 4250 2200 4350
Wire Wire Line
	2200 4900 2200 5000
$Comp
L Connector_Generic:Conn_02x03_Row_Letter_Last J19
U 1 1 5F278985
P 2400 5650
F 0 "J19" H 2450 5967 50  0000 C CNN
F 1 "Conn_02x03_Row_Letter_Last" H 2450 5876 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 2400 5650 50  0001 C CNN
F 3 "~" H 2400 5650 50  0001 C CNN
	1    2400 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 5550 2200 5650
Wire Wire Line
	2200 5650 2200 5750
Connection ~ 2200 5650
$Comp
L Connector_Generic:Conn_02x04_Row_Letter_Last J15
U 1 1 5F27898E
P 4250 4350
F 0 "J15" H 4300 4667 50  0000 C CNN
F 1 "Conn_02x03_Row_Letter_Last" H 4300 4576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 4250 4350 50  0001 C CNN
F 3 "~" H 4250 4350 50  0001 C CNN
	1    4250 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 4250 4050 4350
Wire Wire Line
	4050 4350 4050 4450
Connection ~ 4050 4350
$Comp
L Connector_Generic:Conn_02x04_Row_Letter_Last J17
U 1 1 5F278997
P 4250 4900
F 0 "J17" H 4300 5217 50  0000 C CNN
F 1 "Conn_02x03_Row_Letter_Last" H 4300 5126 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 4250 4900 50  0001 C CNN
F 3 "~" H 4250 4900 50  0001 C CNN
	1    4250 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 4800 4050 4900
Wire Wire Line
	4050 4900 4050 5000
Connection ~ 4050 4900
Text GLabel 4550 5000 2    50   BiDi ~ 0
RXD_GPS
Text GLabel 4550 4450 2    50   BiDi ~ 0
TXD_GPS
Text GLabel 4550 4350 2    50   BiDi ~ 0
TXD-PM
Text GLabel 2700 5550 2    50   Input ~ 0
SS_RAS
$Comp
L Connector_Generic:Conn_02x04_Row_Letter_Last J18
U 1 1 5F2789A4
P 4250 5600
F 0 "J18" H 4300 5917 50  0000 C CNN
F 1 "Conn_02x03_Row_Letter_Last" H 4300 5826 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 4250 5600 50  0001 C CNN
F 3 "~" H 4250 5600 50  0001 C CNN
	1    4250 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 5500 4050 5600
Wire Wire Line
	4050 5600 4050 5700
Connection ~ 4050 5600
Wire Wire Line
	4050 5700 4050 5800
Connection ~ 4050 5700
Wire Wire Line
	4050 4450 4050 4550
Connection ~ 4050 4450
Wire Wire Line
	4050 5000 4050 5100
Connection ~ 4050 5000
Text GLabel 4550 5800 2    50   Input ~ 0
ALERT
$Comp
L power:GND #PWR022
U 1 1 5F6B9EE4
P 4150 1900
F 0 "#PWR022" H 4150 1650 50  0001 C CNN
F 1 "GND" H 4155 1727 50  0000 C CNN
F 2 "" H 4150 1900 50  0001 C CNN
F 3 "" H 4150 1900 50  0001 C CNN
	1    4150 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F4F959D
P 8425 2700
AR Path="/5F4F959D" Ref="C?"  Part="1" 
AR Path="/5EDCDBA8/5F4F959D" Ref="C?"  Part="1" 
AR Path="/5EFF2438/5F4F959D" Ref="C?"  Part="1" 
AR Path="/5F0B8C7C/5F4F959D" Ref="C5"  Part="1" 
AR Path="/5F0E5853/5F4F959D" Ref="C?"  Part="1" 
AR Path="/5F139938/5F4F959D" Ref="C?"  Part="1" 
F 0 "C5" H 8485 2815 59  0000 L BNN
F 1 "100nF" H 8485 2615 59  0000 L BNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8425 2700 50  0001 C CNN
F 3 "" H 8425 2700 50  0001 C CNN
	1    8425 2700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8425 3000 8425 2850
$Comp
L power:GND #GND03
U 1 1 5F273DF7
P 725 1450
AR Path="/5F0B8C7C/5F273DF7" Ref="#GND03"  Part="1" 
AR Path="/5F0E5853/5F273DF7" Ref="#GND?"  Part="1" 
AR Path="/5F139938/5F273DF7" Ref="#GND?"  Part="1" 
F 0 "#GND03" H 725 1450 50  0001 C CNN
F 1 "GND" H 725 1329 59  0000 C CNN
F 2 "" H 725 1450 50  0001 C CNN
F 3 "" H 725 1450 50  0001 C CNN
	1    725  1450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F273DFD
P 725 1150
AR Path="/5F273DFD" Ref="C?"  Part="1" 
AR Path="/5EDCDBA8/5F273DFD" Ref="C?"  Part="1" 
AR Path="/5EFF2438/5F273DFD" Ref="C?"  Part="1" 
AR Path="/5F0B8C7C/5F273DFD" Ref="C6"  Part="1" 
AR Path="/5F0E5853/5F273DFD" Ref="C?"  Part="1" 
AR Path="/5F139938/5F273DFD" Ref="C?"  Part="1" 
F 0 "C6" H 785 1265 59  0000 L BNN
F 1 "100nF" H 785 1065 59  0000 L BNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 725 1150 50  0001 C CNN
F 3 "" H 725 1150 50  0001 C CNN
	1    725  1150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	725  1450 725  1300
Wire Wire Line
	1600 950  725  950 
Wire Wire Line
	725  950  725  1000
Connection ~ 1600 950 
$Comp
L power:+3.3V #PWR0101
U 1 1 5F29C519
P 7150 2150
F 0 "#PWR0101" H 7150 2000 50  0001 C CNN
F 1 "+3.3V" H 7165 2323 50  0000 C CNN
F 2 "" H 7150 2150 50  0001 C CNN
F 3 "" H 7150 2150 50  0001 C CNN
	1    7150 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 2150 8425 2150
$EndSCHEMATC
