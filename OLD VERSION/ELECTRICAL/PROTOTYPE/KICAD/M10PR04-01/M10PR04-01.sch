EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "M10PRO-01"
Date "2020-10-22"
Rev "01"
Comp "M10CUBE"
Comment1 ""
Comment2 "Vero area"
Comment3 "Breadboard area"
Comment4 "Prototype board"
$EndDescr
Text Label 4550 1050 2    50   ~ 0
GPIO_14
Text Label 4550 1150 2    50   ~ 0
GPIO_15
Text Label 4550 1250 2    50   ~ 0
GPIO_18
Text Label 4550 2450 2    50   ~ 0
GPIO_16
Text Label 4550 1950 2    50   ~ 0
GPIO_7
Text Label 4550 1850 2    50   ~ 0
GPIO_8
Wire Wire Line
	950  1450 2150 1450
Wire Wire Line
	950  1350 2150 1350
Text Label 4550 2650 2    50   ~ 0
GPIO_21
Text Label 4550 2550 2    50   ~ 0
GPIO_20
Text Label 4550 2250 2    50   ~ 0
GPIO_12
Text Label 4550 1750 2    50   ~ 0
GPIO_25
Text Label 4550 1450 2    50   ~ 0
GPIO_24
Text Label 4550 1550 2    50   ~ 0
GPIO_23
Text Label 950  2550 0    50   ~ 0
GPIO_26
Text Label 950  2450 0    50   ~ 0
GPIO_19
Text Label 950  2350 0    50   ~ 0
GPIO_13
Text Label 950  2250 0    50   ~ 0
GPIO_6
Text Label 950  2150 0    50   ~ 0
GPIO_5
Text Label 950  1350 0    50   ~ 0
GPIO_22
Text Label 950  1450 0    50   ~ 0
GPIO_27
Text Label 950  1250 0    50   ~ 0
GPIO17
Text Label 950  1050 0    50   ~ 0
GPIO4
NoConn ~ 4550 1150
NoConn ~ 4550 1050
NoConn ~ 950  1450
NoConn ~ 950  1350
NoConn ~ 950  1050
Wire Wire Line
	950  1050 2150 1050
Wire Wire Line
	1650 750  950  750 
Connection ~ 1650 750 
Text Label 4550 850  2    50   ~ 0
5V_BUS
Text Label 4550 750  2    50   ~ 0
5V_BUS
$Comp
L power:+3.3V #PWR07
U 1 1 5FE129A9
P 1650 750
F 0 "#PWR07" H 1650 600 50  0001 C CNN
F 1 "+3.3V" V 1665 878 50  0000 L CNN
F 2 "" H 1650 750 50  0001 C CNN
F 3 "" H 1650 750 50  0001 C CNN
	1    1650 750 
	1    0    0    -1  
$EndComp
Text Label 950  950  0    50   ~ 0
SCL_BUS
Text Label 950  850  0    50   ~ 0
SDA_BUS
Text Label 950  750  0    50   ~ 0
3V3_BUS
Text Label 950  1550 0    50   ~ 0
3V3_2
$Comp
L power:GND #PWR016
U 1 1 5F5B07A3
P 4550 2350
F 0 "#PWR016" H 4550 2100 50  0001 C CNN
F 1 "GND" V 4555 2222 50  0000 R CNN
F 2 "" H 4550 2350 50  0001 C CNN
F 3 "" H 4550 2350 50  0001 C CNN
	1    4550 2350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5F5AFC62
P 4550 2150
F 0 "#PWR015" H 4550 1900 50  0001 C CNN
F 1 "GND" V 4555 2022 50  0000 R CNN
F 2 "" H 4550 2150 50  0001 C CNN
F 3 "" H 4550 2150 50  0001 C CNN
	1    4550 2150
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5F5AF4BD
P 4550 1650
F 0 "#PWR014" H 4550 1400 50  0001 C CNN
F 1 "GND" V 4555 1522 50  0000 R CNN
F 2 "" H 4550 1650 50  0001 C CNN
F 3 "" H 4550 1650 50  0001 C CNN
	1    4550 1650
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5F5AE6B8
P 4550 1350
F 0 "#PWR013" H 4550 1100 50  0001 C CNN
F 1 "GND" V 4555 1222 50  0000 R CNN
F 2 "" H 4550 1350 50  0001 C CNN
F 3 "" H 4550 1350 50  0001 C CNN
	1    4550 1350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5F5ADC88
P 950 2650
F 0 "#PWR03" H 950 2400 50  0001 C CNN
F 1 "GND" V 955 2522 50  0000 R CNN
F 2 "" H 950 2650 50  0001 C CNN
F 3 "" H 950 2650 50  0001 C CNN
	1    950  2650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5F5ABB16
P 950 1950
F 0 "#PWR02" H 950 1700 50  0001 C CNN
F 1 "GND" V 955 1822 50  0000 R CNN
F 2 "" H 950 1950 50  0001 C CNN
F 3 "" H 950 1950 50  0001 C CNN
	1    950  1950
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5F5AB3CF
P 950 1150
F 0 "#PWR01" H 950 900 50  0001 C CNN
F 1 "GND" V 955 1022 50  0000 R CNN
F 2 "" H 950 1150 50  0001 C CNN
F 3 "" H 950 1150 50  0001 C CNN
	1    950  1150
	0    1    1    0   
$EndComp
$Comp
L raspberrypi_hat:OX40HAT J1
U 1 1 58DFC771
P 2750 750
F 0 "J1" H 3100 850 50  0000 C CNN
F 1 "40HAT" H 2450 850 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Vertical" H 2750 950 50  0001 C CNN
F 3 "" H 2050 750 50  0000 C CNN
	1    2750 750 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 2650 950  2650
Text Label 950  2050 0    50   ~ 0
ID_SDA
Wire Wire Line
	2150 2050 950  2050
Wire Wire Line
	2150 1950 950  1950
Wire Wire Line
	2150 1150 950  1150
Wire Wire Line
	2150 750  1650 750 
Wire Wire Line
	3350 1350 4550 1350
Wire Wire Line
	3350 1650 4550 1650
Wire Wire Line
	3350 2050 4550 2050
Wire Wire Line
	3350 2150 4550 2150
Wire Wire Line
	3350 2350 4550 2350
Text Label 4550 2050 2    50   ~ 0
ID_SCL
Text Label 4550 950  2    50   ~ 0
GND
Wire Wire Line
	3350 950  4550 950 
Wire Wire Line
	3350 850  4550 850 
Wire Wire Line
	3350 750  4550 750 
Wire Wire Line
	2150 1550 950  1550
Wire Wire Line
	2150 1650 950  1650
Wire Wire Line
	2150 1750 950  1750
Wire Wire Line
	2150 1850 950  1850
Wire Wire Line
	2150 2150 950  2150
Wire Wire Line
	2150 2250 950  2250
Wire Wire Line
	2150 1250 950  1250
Wire Wire Line
	2150 850  950  850 
Wire Wire Line
	2150 950  950  950 
Wire Wire Line
	3350 1050 4550 1050
Wire Wire Line
	950  2450 2150 2450
Wire Wire Line
	950  2350 2150 2350
Wire Wire Line
	3350 1750 4550 1750
Wire Wire Line
	4550 2550 3350 2550
Wire Wire Line
	4550 2650 3350 2650
Wire Wire Line
	3350 1550 4550 1550
Wire Wire Line
	3350 1450 4550 1450
Wire Wire Line
	3350 2450 4550 2450
Wire Wire Line
	3350 2250 4550 2250
Wire Wire Line
	3350 1950 4550 1950
Wire Wire Line
	3350 1850 4550 1850
Wire Wire Line
	3350 1250 4550 1250
Wire Wire Line
	3350 1150 4550 1150
NoConn ~ 950  1250
NoConn ~ 950  1650
NoConn ~ 950  1750
NoConn ~ 950  1850
NoConn ~ 950  2150
NoConn ~ 950  2250
NoConn ~ 950  2350
NoConn ~ 4550 1250
NoConn ~ 4550 1450
NoConn ~ 4550 1550
NoConn ~ 4550 1750
NoConn ~ 4550 1850
NoConn ~ 4550 2250
NoConn ~ 4550 2450
NoConn ~ 4550 2550
NoConn ~ 950  2450
Wire Wire Line
	2150 2550 950  2550
NoConn ~ 4550 2650
NoConn ~ 4550 1950
NoConn ~ 950  1550
NoConn ~ 950  2550
$Comp
L Mechanical:MountingHole H1
U 1 1 5F6676D5
P 9575 950
F 0 "H1" H 9675 996 50  0000 L CNN
F 1 "MountingHole" H 9675 905 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad_Via" H 9575 950 50  0001 C CNN
F 3 "~" H 9575 950 50  0001 C CNN
	1    9575 950 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F668877
P 9575 1200
F 0 "H2" H 9675 1246 50  0000 L CNN
F 1 "MountingHole" H 9675 1155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad_Via" H 9575 1200 50  0001 C CNN
F 3 "~" H 9575 1200 50  0001 C CNN
	1    9575 1200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F668E2C
P 9575 1450
F 0 "H3" H 9675 1496 50  0000 L CNN
F 1 "MountingHole" H 9675 1405 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad_Via" H 9575 1450 50  0001 C CNN
F 3 "~" H 9575 1450 50  0001 C CNN
	1    9575 1450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5F669B29
P 9575 1725
F 0 "H4" H 9675 1771 50  0000 L CNN
F 1 "MountingHole" H 9675 1680 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad_Via" H 9575 1725 50  0001 C CNN
F 3 "~" H 9575 1725 50  0001 C CNN
	1    9575 1725
	1    0    0    -1  
$EndComp
$EndSCHEMATC
