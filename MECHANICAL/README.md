<div><p align="center"><img src="/MECHANICAL/IMAGES/top.jpg"> <img src="/MECHANICAL/IMAGES/middle.jpg"> <img src="/MECHANICAL/IMAGES/bottom.jpg"></div>
<p>
<p><H1>NEW PROPOSED DIMENTIONS for M10CUBE FRAMEWORK</H1></p>
These propossed dimentions are experimental, printed and see how it goes after discussion.<br>
External CUBE dimensions remains the same:100x100x100 mm. Enclosure is made of three parts<p>
1 - Bottom 15mm<br>
2 - Middle 15mm<br>
3 - Top 15 mm<br>
<p>
OLD PROPOSED DIMENTIONS for M10CUBE FRAMEWORK<br>
Total M10CUBE will be      :1 CPU frame + 4 add on frames 29.6 + 4x17.6 = 100mm<br>
External CUBE dimensions :100x100x100 mm. Enclosure is made of three parts<p>
- Base (CPU) Frame width   :29.6 mm<br>
- Add on frame width       :17.6<br>
- PCB inside frames        :90x90 mm<br>

<a href="https://youtu.be/5b76yTjtDQU?t=8" target="_blank"><b>video</b></a> 43sec <p>
<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<p> 

The wizcube idea is presented in <a href="https://wizcube.eu" target="_blank"><b>WIZcube</b></a> as participating in <b>"WIZnet Ethernet HAT contest 2022 for Raspberry Pi Pico and RP2040 projects"</b>
