<b>WIZcube CODESYS</b><p>

Here can be found PLC programs in CODESYS (https://codesys.com) environment using various WIZcube modules.<p>

<b>Copied from Wikipedia</b><br>
CODESYS is a development environment for programming controller applications according to the international industrial standard IEC 61131-3. The main product of the software suite is the CODESYS Development System, an IEC 61131-3 tool.<p>

Introduction<br>
CODESYS is developed and marketed by the German software company CODESYS GmbH located in the Bavarian town of Kempten. The company was founded in 1994 under the name 3S-Smart Software Solutions – it was renamed in 2018 and 2020. Version 1.0 of CODESYS was released in 1994. Licenses of the CODESYS Development System are free of charge and can be installed legally without copy protection on further workstations. The software suite covers different aspects of industrial automation technology with one surface. The tool is independent from device manufacturers and thus used for hundreds of different controllers, PLCs (programmable logic controllers), PAC (programmable automation controllers), ECUs (electronic control units), controllers for building automation and other programmable controllers mostly for industrial purposes. 

Licensing:<p>

Single Device License<br>

CODESYS Control for Raspberry Pi SL contains a CODESYS Control application for all Raspberry Pi, including the Compute Module (see http://www.raspberrypi.org/) as well as the capability to use the extension modules and several devices/breakouts with SPI, I²C or 1-wire communication interface.<p>

This product can be installed with the included CODESYS Deploy Tool plug-in via the CODESYS Development System on a Linux distribution Raspbian. After each restart the runtime system will be started automatically. If no valid full license can be found, CODESYS Control runs for two hours without functional limitations before shut down.<p>

Detailed information can be found in the <a href="https://help.codesys.com/webapp/_rbp_f_help;product=CODESYS_Control_for_Raspberry_Pi_SL" target="_blank"><b>Online Help</b></a>

<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br> 





