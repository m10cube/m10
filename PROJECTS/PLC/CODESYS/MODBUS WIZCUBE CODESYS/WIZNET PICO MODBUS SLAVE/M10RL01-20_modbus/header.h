#include <EthernetClient.h>
#include <Ethernet.h>
#include <SPI.h>
#include <ArduinoModbus.h>
#include <ArduinoRS485.h>
#include "BUZZER.h"
#define BUZZER 1
  
  //Define OUTPUT pins on Wiznet Pico clone
  int OUT_1 = 4;
  int OUT_2 = 5;
  int OUT_3 = 6;
  int OUT_4 = 7;
  int OUT_5 = 8;
  int OUT_6 = 9;
  int OUT_7 = 10;
  int OUT_8 = 11;
  
  byte mac[] = {  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};  // Define MAC address
  int gpio[] = {0,0,0,0,0,0,0,0,OUT_1,OUT_2,OUT_3,OUT_4,OUT_5,OUT_6,OUT_7,OUT_8};
  IPAddress ip(192, 168, 1, 214);
  EthernetServer ethServer(502);

/** MODBUS **/
ModbusTCPServer modbusTCPServer;

/** DATABASE **/
typedef struct  {
  bool condition; 
} database_t;
  database_t  state_db[16];
