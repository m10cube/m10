PROGRAM program0
  VAR
    LED1 AT %QX0.4 : BOOL;
    LED2 AT %QX0.6 : BOOL;
    LED3 AT %QX0.8 : BOOL;
    LED4 AT %QX0.10 : BOOL;
    LED5 AT %QX0.12 : BOOL;
    LED6 AT %QX0.14 : BOOL;
    LED7 AT %QX0.26 : BOOL;
    LED8 AT %QX0.28 : BOOL;
    SW1 AT %IX0.5 : BOOL;
    SW2 AT %IX0.7 : BOOL;
    SW3 AT %IX0.9 : BOOL;
    SW4 AT %IX0.11 : BOOL;
    SW5 AT %IX0.13 : BOOL;
    SW6 AT %IX0.15 : BOOL;
    SW7 AT %IX0.22 : BOOL;
    SW8 AT %IX0.27 : BOOL;
  END_VAR
  VAR
    TOF1 : TOF;
    TOF2 : TOF;
    TOF3 : TOF;
    TOF4 : TOF;
  END_VAR

  TOF1(IN := SW1, PT := T#1s);
  LED1 := NOT(TOF1.Q);
  TOF2(IN := SW2, PT := T#2s);
  LED2 := NOT(TOF2.Q);
  TOF3(IN := SW3, PT := T#3s);
  LED3 := NOT(TOF3.Q);
  TOF4(IN := SW4, PT := T#4s);
  LED4 := NOT(TOF4.Q);
  LED5 := NOT(SW5);
  LED6 := NOT(SW6);
  LED7 := NOT(SW7);
  LED8 := NOT(SW8);
END_PROGRAM


CONFIGURATION config

  RESOURCE Resource1 ON PLC
    TASK PLCTask(INTERVAL := T#5ms,PRIORITY := 0);
    PROGRAM Instance1 WITH PLCTask : program0;
  END_RESOURCE
END_CONFIGURATION
