/**********************************************************************/
/* This file has been generated automatically by Beremiz4Pico Manager */
/* WARNING : DO NOT EDIT MANUALLY THIS FILE                           */
/**********************************************************************/

#include "iec_types.h"
#include <Arduino.h>
#include <Servo.h>

uint8_t DigitalInput[30];
uint8_t DigitalOutput[30];

IEC_BOOL* __QX0_4=&DigitalOutput[4];
IEC_BOOL* __IX0_5=&DigitalInput[5];
IEC_BOOL* __QX0_6=&DigitalOutput[6];
IEC_BOOL* __IX0_7=&DigitalInput[7];
IEC_BOOL* __QX0_8=&DigitalOutput[8];
IEC_BOOL* __IX0_9=&DigitalInput[9];
IEC_BOOL* __QX0_10=&DigitalOutput[10];
IEC_BOOL* __IX0_11=&DigitalInput[11];
IEC_BOOL* __QX0_12=&DigitalOutput[12];
IEC_BOOL* __IX0_13=&DigitalInput[13];
IEC_BOOL* __QX0_14=&DigitalOutput[14];
IEC_BOOL* __IX0_15=&DigitalInput[15];
IEC_BOOL* __IX0_22=&DigitalInput[22];
IEC_BOOL* __QX0_26=&DigitalOutput[26];
IEC_BOOL* __IX0_27=&DigitalInput[27];
IEC_BOOL* __QX0_28=&DigitalOutput[28];

void ConfigurePLCIO (void)
{
  pinMode (4, OUTPUT);
  pinMode (5, INPUT);
  pinMode (6, OUTPUT);
  pinMode (7, INPUT);
  pinMode (8, OUTPUT);
  pinMode (9, INPUT);
  pinMode (10, OUTPUT);
  pinMode (11, INPUT);
  pinMode (12, OUTPUT);
  pinMode (13, INPUT);
  pinMode (14, OUTPUT);
  pinMode (15, INPUT);
  pinMode (22, INPUT);
  pinMode (26, OUTPUT);
  pinMode (27, INPUT);
  pinMode (28, OUTPUT);
}

void AcquirePLCInputs (void)
{
  DigitalInput[5]=digitalRead(5);
  DigitalInput[7]=digitalRead(7);
  DigitalInput[9]=digitalRead(9);
  DigitalInput[11]=digitalRead(11);
  DigitalInput[13]=digitalRead(13);
  DigitalInput[15]=digitalRead(15);
  DigitalInput[22]=digitalRead(22);
  DigitalInput[27]=digitalRead(27);
}

void UpdatePLCOutputs (void)
{
  digitalWrite (4, DigitalOutput[4]);
  digitalWrite (6, DigitalOutput[6]);
  digitalWrite (8, DigitalOutput[8]);
  digitalWrite (10, DigitalOutput[10]);
  digitalWrite (12, DigitalOutput[12]);
  digitalWrite (14, DigitalOutput[14]);
  digitalWrite (26, DigitalOutput[26]);
  digitalWrite (28, DigitalOutput[28]);
}
