<b>PROJECT:  DISHWASHER</b><br>
Our dishwasher machine with a broken controller, will be modified  to use one of our WIZcube module to do the job.
<p>Work in progress. Nightly builds for the firmware regurally.</p>
<div><p align="center"><img src="dishwasher_codesys.jpg"></p></div>
WIZcube modules that can be part of the CNC CONTROLLER are:<br>
<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/INPUT%20OUTPUT/M10DX02-20" target="_blank"><b>M10DX02-20</b></a> 8X 24V INPUT 8XTRIAC OUTPUT<br>
<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/HMI" target="_blank"><b>M10HM01-20</b></a> A 24V / 5V Power Supply Unit<br>
<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/PSU" target="_blank"><b>M10PS01-01</b></a> 8X 24V INPUT 8XTRIAC OUTPUT<br>
<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br> 





