
void Pump_Drain_First(){
  drain_working = true;        //DRAIN PROCEDURE STARTED
  if (drain_start){
    state_db[pump_drain_valve].condition = 1; //DRAIN ON
     //Serial.println("DRAIN ON ");
  }
   ton_1.process(1);
    r_trig_1.process(ton_1.Q);
  if (r_trig_1.Q){
    drain_start = false;   //RESET DRAIN START INITIATE
    state_db[pump_drain_valve].condition = 0; //DRAIN OFF
    //Serial.println("DRAIN OFF ");
    ton_1.process(0); 
    ton_2.process(1);
    r_trig_2.process(ton_2.Q);
  }
  if (r_trig_2.Q){
    state_db[pump_drain_valve].condition = 1; //DRAIN ON   
    //Serial.println("DRAIN ON "); 
    ton_3.process(1);
    r_trig_3.process(ton_3.Q);
  }
  if (r_trig_3.Q){
    state_db[pump_drain_valve].condition = 0; //DRAIN OFF   
    //Serial.println("DRAIN OFF ");
    drain_working = false;      //DRAIN PROCEDURE ENDED
    drain_end = true;      //DRAIN PROCEDURE ENDED
    
    circulating_flag = true;
    //drain_init();
  }
}

void Pump_Drain_Last(){
    ton_11.process(1);
    r_trig_11.process(ton_11.Q);
  if (r_trig_11.Q){    
    state_db[pump_drain_valve].condition = 0; //DRAIN ON
    Serial.println("1 DRAIN ON ");
    ton_11.process(0); 
    ton_12.process(1);
    r_trig_12.process(ton_12.Q);
  }
  if (r_trig_12.Q){
    state_db[pump_drain_valve].condition = 1; //DRAIN OFF   
    Serial.println("2 DRAIN OFF ");
    ton_12.process(0); 
    ton_13.process(1);
    r_trig_13.process(ton_13.Q);
  }
  if (r_trig_13.Q){
    state_db[pump_drain_valve].condition = 0; //DRAIN ON  
    Serial.println("3 DRAIN ON "); 
    ton_13.process(0);
    ton_14.process(1);
    r_trig_14.process(ton_14.Q);
  }
if (r_trig_14.Q){
    state_db[pump_drain_valve].condition = 1; //DRAIN OFF   
    Serial.println("4 DRAIN OFF ");
    ton_14.process(0); 
    ton_15.process(1);
    r_trig_15.process(ton_15.Q);
  }
  if (r_trig_15.Q){
    state_db[pump_drain_valve].condition = 0; //DRAIN OFF   
    drain_working = false;      //DRAIN PROCEDURE ENDED
    circulating_flag = true;
    drain_init();
  }
}
