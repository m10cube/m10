#ifndef header_h
#define header_h

#include <EthernetClient.h>
#include <Ethernet.h>
#include <SPI.h>
#include <ArduinoModbus.h>
#include <ArduinoRS485.h>
#include "Free_Fonts.h"
#include <TFT_eSPI.h>
#include <string.h>
#include <plclib.h>
#include <AutoPID.h>
#include <DallasTemperature.h>
#include <OneWire.h>
#include "BUZZER.h"

#define BUZZER 1

#endif   

//HEATER
//pins

#define TEMP_PROBE_PIN 22// INP_7 MUST NOT USED

#define TEMP_READ_DELAY 800 //can only read digital temp sensor every ~750ms

//pid settings and gains
#define OUTPUT_MIN 0
#define OUTPUT_MAX 255
#define KP .12
#define KI .0003
#define KD 0

double temperature, setPoint, outputVal;
OneWire oneWire(TEMP_PROBE_PIN);
DallasTemperature temperatureSensors(&oneWire);

//input/output variables passed by reference, so they are updated automatically
AutoPID myPID(&temperature, &setPoint, &outputVal, OUTPUT_MIN, OUTPUT_MAX, KP, KI, KD);

unsigned long lastTempUpdate; //tracks clock time of last temp update

//call repeatedly in loop, only updates after a certain time interval
//returns true if update happened


//Define INPUT pins on Wiznet Pico clone
int IN_1 = 5;
int IN_2 = 7;
int IN_3 = 9;
int IN_4 = 11;
int IN_5 = 13;
int IN_6 = 15;
int IN_7 = 22;
int IN_8 = 27;

//Define OUTPUT pins on Wiznet Pico clone
int OUT_1 = 4;
int OUT_2 = 6;
int OUT_3 = 8;
int OUT_4 = 10;
int OUT_5 = 12;
int OUT_6 = 14;
int OUT_7 = 26;
int OUT_8 = 28;





//Define INPUT pins on Wiznet Pico clone
#define start_in 0                 //INPUT 1
#define stop_button 1              //INPUT 2
#define water_level 2              //INPUT 3
#define tacho_pin 3               //INPUT 4

/*
 * THIS IS THE CORRECT SETUP
//Define OUTPUT pins on Wiznet Pico clone
#define start_out 4                   //OUTPUT 1
#define heater 6                      //OUTPUT 2
#define pump_drain 8                  //OUTPUT 3
#define pump_circulating 10           //OUTPUT 4
#define valve_regenerative 12         //OUTPUT 5
#define valve_cold 14                 //OUTPUT 6
#define valve_detergent_dispencer 26  //OUTPUT 7
*/


//* THIS IS THE TEST SETUP
#define start_relay 8                       //OUTPUT 6
#define pump_drain_valve 13                 //OUTPUT 6
#define pump_circulating_valve 14           //OUTPUT 7
#define heater_relay 15                     //OUTPUT 8


boolean start_button;
boolean start_button_1;
boolean drain_start;
boolean drain_working;
boolean drain_end;
boolean heater_flag;
boolean circulating_flag;
boolean circulate;
int circulating_count;
int drain_count;



/** ETHERNET **/
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xE0};  // Define MAC address
int gpio[] = {IN_1,IN_2,IN_3,IN_4,IN_5,IN_6,IN_7,IN_8,OUT_1,OUT_2,OUT_3,OUT_4,OUT_5,OUT_6,OUT_7,OUT_8};
IPAddress ip(192, 168, 1, 214);
EthernetServer ethServer(502);

/** MODBUS **/
ModbusTCPServer modbusTCPServer;


/** DATABASE **/
typedef struct  {
  bool condition; 
} database_t;
  database_t  state_db[16];

TON ton_1(4000);
TON ton_2(1000);
TON ton_3(2000);
TON ton_4(1000);
TON ton_5(1000);
TON ton_6(1000);
TON ton_7(1000);
TON ton_8(1000);
TON ton_9(1000);
TON ton_10(1000);

TON ton_11(2000);
TON ton_12(2000);
TON ton_13(2000);
TON ton_14(2000);
TON ton_15(2000);
TON ton_16(1000);
TON ton_17(1000);
TON ton_18(1000);
TON ton_19(1000);
TON ton_20(1000);

TON ton_21(300);
TON ton_22(500);
TON ton_23(500);
TON ton_24(500);
TON ton_25(500);
TON ton_26(500);
TON ton_27(500);
TON ton_28(500);
TON ton_29(500);
TON ton_30(500);


R_TRIG r_trig_START;
R_TRIG r_trig_START_CIRCULATING;
R_TRIG r_trig_1;
R_TRIG r_trig_2;
R_TRIG r_trig_3;
R_TRIG r_trig_4;
R_TRIG r_trig_5;
R_TRIG r_trig_6;
R_TRIG r_trig_7;
R_TRIG r_trig_8;
R_TRIG r_trig_9;
R_TRIG r_trig_10;

R_TRIG r_trig_11;
R_TRIG r_trig_12;
R_TRIG r_trig_13;
R_TRIG r_trig_14;
R_TRIG r_trig_15;
R_TRIG r_trig_16;
R_TRIG r_trig_17;
R_TRIG r_trig_18;
R_TRIG r_trig_19;
R_TRIG r_trig_20;

R_TRIG r_trig_21;
R_TRIG r_trig_22;
R_TRIG r_trig_23;
R_TRIG r_trig_24;
R_TRIG r_trig_25;
R_TRIG r_trig_26;
R_TRIG r_trig_27;
R_TRIG r_trig_28;
R_TRIG r_trig_29;
R_TRIG r_trig_30;



F_TRIG f_trig_1;
F_TRIG f_trig_2;
F_TRIG f_trig_3;
F_TRIG f_trig_4;
F_TRIG f_trig_5;
F_TRIG f_trig_6;
