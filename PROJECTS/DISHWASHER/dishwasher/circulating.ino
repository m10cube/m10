void pump_circulating(){
   ton_21.process(1);
   r_trig_21.process(ton_21.Q);
  if (r_trig_21.Q){    
    state_db[pump_circulating_valve].condition = 1; //DRAIN ON
    //Serial.println("1 DRAIN ON ");
    ton_21.process(0); 
    ton_22.process(1);
    r_trig_22.process(ton_22.Q);
  }
  if (r_trig_22.Q){
    state_db[pump_circulating_valve].condition = 0; //DRAIN OFF   
    //Serial.println("2 DRAIN OFF ");
    ton_22.process(0); 
    ton_23.process(1);
    r_trig_23.process(ton_23.Q);
  }
    if (r_trig_23.Q){
      ton_23.process(0);
      circulating_count++;
      if (circulating_count > 20){
        circulating_flag = false;
      }
     circulating_init();
    }
}
