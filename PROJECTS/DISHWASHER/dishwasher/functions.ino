
void dishwasher_cycle(){     
    if (state_db[start_in].condition){
      dishwasher_on();
    } 

    if (!drain_working && (state_db[start_in].condition)){
        drain_start = true; //SET ONCE
      }
   
    if (drain_start || drain_working and !drain_end){
      Pump_Drain_First();       //DRAIN CONTROLL
     }

    if (circulating_flag){
      heater_flag = true;
      pump_circulating();       //CIRCULATING PUMP CONTROL      
    }  
    if (heater_flag){
      heater_control();       //HEATER CONTROL
    }
}

void dishwasher_on(){
  state_db[start_relay].condition = state_db[start_in].condition;
  //Serial.println("START OUT IS ON ");
}

void in_out_test(){
  int n = 1;
  while (n < 100){
  Serial.println(n);
    state_db[8].condition = (state_db[0].condition);
    Serial.print("INPUT 1: = ");
    Serial.println(state_db[0].condition);
    state_db[9].condition = (state_db[1].condition);
    Serial.print("INPUT 2: = ");
    Serial.println(state_db[1].condition);
    state_db[10].condition = (state_db[2].condition);
    Serial.print("INPUT 3: = ");
    Serial.println(state_db[2].condition);
    //state_db[11].condition = (state_db[3].condition);
    Serial.print("INPUT 4: = ");
    Serial.println(state_db[3].condition);   
    state_db[12].condition = (state_db[4].condition);
    Serial.print("INPUT 5: = ");
    Serial.println(state_db[4].condition);
    state_db[13].condition = (state_db[5].condition);  
    Serial.print("INPUT 6: = ");
    Serial.println(state_db[5].condition);
    state_db[14].condition = (state_db[6].condition);  
    Serial.print("INPUT 7: = ");
    Serial.println(state_db[6].condition);
    state_db[15].condition = (state_db[7].condition);  
    Serial.print("INPUT 8: = ");
    Serial.println(state_db[7].condition);
    Serial.println();
    refresh_input();
    refresh_output();
    #if BUZZER
     cricket(2); // Initialisation OK sound
    #endif  
    delay(1000);
    n++;
  }
}

void config_gpio(){
  for(int i = 0; i < 8; i++) {
    pinMode(gpio[i], INPUT);
    pinMode(gpio[i + 8], OUTPUT);
  }       
}

void refresh_input(void){
  for(int i = 0; i < 8; i++) {
    state_db[i].condition = !digitalRead(gpio[i]);
  } 
}

void refresh_output(void){
  for(int i = 0; i < 8; i++) {
    digitalWrite(gpio[i + 8], state_db[i + 8].condition);
  }
}

void drain_init(){
      ton_1.process(0);
      ton_2.process(0);
      ton_3.process(0);
      ton_4.process(0);
      ton_5.process(0);
      
      ton_11.process(0);
      ton_12.process(0);
      ton_13.process(0);
      ton_14.process(0);
      ton_15.process(0);  
}

void circulating_init(){
      ton_21.process(0);
      ton_22.process(0);
      ton_23.process(0); 
      ton_24.process(0);
      ton_25.process(0);
}



void refresh_io(){
  refresh_input();        //Read input stage to record [0]     
  refresh_output();       //Write record [1] to output stage
}

void set_outputs(){
  for(int i = 0; i < 8; i++) {
    digitalWrite(gpio[i + 8], 1);
  }
}

void reset_outputs(){
  for(int i = 0; i < 8; i++) {
    digitalWrite(gpio[i + 8], 0);
  }
}

void flash_outputs(){
  set_outputs();
  delay(1000);
  reset_outputs();
  delay(1000);
}
