<b>PROJECT: CNC CONTROLLER</b><br>

<div><p align="center">An artistic impression of the CNC Controller builded upon WIZcube hardware</p></div>
<div><p align="center"><img src="CNC.jpg"></p></div>
The project in intended to be a <a href="https://github.com/grblHAL/RP2040/discussions/18"  target="_blank"><b>UNIVERSAL CONTROLLER</b></a> for CNC and 3D printer in the futute.<p>
All WIZcube modules are using W5100S-EVB-Pico module and the future release the WiFi version WizFi360-EVB-Pico<p>

This is a 6AXIS CNC CONTROLLER with modified <a href="https://github.com/Volksolive/RP2040" target="_blank"> <b>GRBLHAL</b></a>. Work in progreess by Olivier Gaillard.
This is a huge project and is running almost 99%. It communicates with the M10DX01-20 via TCP/IP Modbus.<br>
The M10DX01-20 module is attached to CNC module with I2C (interrupts supported) and transfer all data stream from
The remote HMI (hand held).<br>
The same M10DX01-20 module is used to transfer Limit Switches to M10NC02-20 (CNC) module and power output to control relays (oiling, vacuum cleaner, lights etc.).<br>
Can combined with the relay module M10RL01-20 or the TRIAC module 10RL01-20.<br>
The program will be written in C, will run locally on the module<br>
The project is posted in Openbuilds as a new build here: <a href="https://openbuilds.com/builds/m10cube-pico-cnc-a-universal-controller.10283/" target="_blank">OPENBUILDS BUILD</a><br>
WIZcube modules that can be part of the CNC CONTROLLER are:<br>

<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/INPUT%20OUTPUT/M10DX01-20" target="_blank"><b>M10DX01-20</b></a> 8X24V INPUT 8X24V OUTPUT HIGH SIDE: Switching large Relay or SSR for heavy loads e.g heating water tanks that require 20 amps load.<br>
<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/OUTPUT/M10RL01-20" target="_blank"><b>M10RL01-20</b></a> 8XRELAY: Control AC or DC loads.<br>
<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/HMI" target="_blank"><b>M10HM01-20</b></a> A modified Human Machine Interface module (HMI) to be used for user interfacing with the CNC central module. We call it Remote control module. RC in short. RC i equiped with rotary encoders to easy the Remote Control of the CNC. Communication via Modbus TCP/IP.<br>
A grate improvement will be the drom in replacement of the <b>W5100S-EVB-Pico</b> module with the new one <b>WizFi360-EVB-Pico</b> with WiFi capabilities. Only a small back of battery inside and we are free from wires meshing arround us. Like the Pro modules.A grate improvement will be the drop in replacement of the W5100S-EVB-Pico module with the new one WizFi360-EVB-Pico with WFi capabilities. Only a small back of battery inside and we are free from wires meshing aground the workshop. Like the Pro modules.

Work in n progress and we believe it will be ready soon to test it in our CNC.<br>

<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br>




