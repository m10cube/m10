/*29/04/2022
www.wizcube.eu
WIZcube Modbus TCP/IP 8XINPUT and 8XOUTPUT Slave module  
Recieved data activates OUTPUT. INPUT state transmitesd to Master
Status of outputs are send back to CODESYS and shownn on visualisation screen as LOOP BACK STATUS  
Write múltiple coils function code 15 is used to tranfer data to OUTPUTS.
Read handling registers function code 3 is used to tranffer data from INPUTS  
INPUTS are stored in INPUT record and then copied to Modbus for sending to CODESYS or WIznet master
INPUT records (REC [0]..[7]) contain a mirror of INPUTS states
OUTPUT records (REC [8]..[15])contain a mirror of OUTPUT states 
*/

#include "header.h"

void setup(){
  Serial.begin(115200); 

  // Init the Ethernet connection
  Ethernet.init(17);    // WIZnet W5100S-EVB-Pico SPI CS
  Ethernet.begin(mac, ip); //start the Ethernet connection and the server:
  Serial.print("MODBUS SERVER at ");
  Serial.println(Ethernet.localIP());
  
  // Start the Ethernet server
  ethServer.begin();
  
  // Start the Modbus TCP server
  if (!modbusTCPServer.begin()) {
    Serial.println("Failed to start Modbus TCP Server!");
    while (1);
  }

  // Configure the GPIOs
  config_gpio();   

  // Configure 8 coils at address 0x00
  modbusTCPServer.configureCoils(0x00, 8);
  // Configure 3 holding registers starting at address 0x00
  modbusTCPServer.configureHoldingRegisters(0x00, 3);
}

void loop()
{
  // Listen for incoming clients8  EthernetClient client = ethServer.available();
  EthernetClient client = ethServer.available();
  
  if(client) {
    // A new client connected
    Serial.println("new client");

    // let the Modbus TCP accept the connection 
    modbusTCPServer.accept(client);

    while(client.connected()) {
      // poll for Modbus TCP requests, while client connected
      modbusTCPServer.poll();

      // update the LED
      send_status();          //send back to CODESYS as LOOP BACK STATUS what is sent as outputs
      refresh_input();        //Read input stage to record [0]
      refresh_input_modbus(); //Fill Modbus input Registers with record [0]
      refresh_output_modbus();//Fill record [1]  with Modbus output registers
      
      refresh_output();       //Write record [1] to output stage
    }

    Serial.println("client disconnected");
  }
}
