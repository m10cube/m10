#include <EthernetClient.h>
#include <Ethernet.h>
#include <SPI.h>
#include <ArduinoModbus.h>
#include <ArduinoRS485.h>

//Define INPUT pins on Wiznet Pico clone
int IN_1 = 5;
int IN_2 = 7;
int IN_3 = 9;
int IN_4 = 11;
int IN_5 = 13;
int IN_6 = 15;
int IN_7 = 22;
int IN_8 = 27;

//Define OUTPUT pins on Wiznet Pico clone
int OUT_1 = 4;
int OUT_2 = 6;
int OUT_3 = 8;
int OUT_4 = 10;
int OUT_5 = 12;
int OUT_6 = 14;
int OUT_7 = 26;
int OUT_8 = 28;


/** ETHERNET **/
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xE0};  // Define MAC address
int gpio[] = {IN_1,IN_2,IN_3,IN_4,IN_5,IN_6,IN_7,IN_8,OUT_1,OUT_2,OUT_3,OUT_4,OUT_5,OUT_6,OUT_7,OUT_8};
IPAddress ip(192, 168, 1, 214);
EthernetServer ethServer(502);

/** MODBUS **/
ModbusTCPServer modbusTCPServer;

/** DATABASE **/
typedef struct  {
  bool condition; 
} database_t;
  database_t  state_db[16];
