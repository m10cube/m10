#ifndef BUZZER_H
#define BUZZER_H
  
  #define GPIO_PWM_NUM 27// 125Mhz / 1000Hz / 2

  void BUZZER_init();
  void BUZZER(int x, int y);
  void cricket(int i);  
  void buzzer_sound(int x, int y, int z);
    
  void BUZZER_init(){
    pinMode(GPIO_PWM_NUM, OUTPUT);
  }

  void cricket(int i){
  int j =0;
   while (j < i){
   int n = 0; 
    while (n < 10){
       buzzer_sound(1, 1000, 2);
      delay(2);
      n++;
    }
    j++;
    delay(100);
  }
}
  
  void buzzer_sound(int x, int y, int z){
   for (int n =0; n < x; n++){
      tone(GPIO_PWM_NUM, y, z);
      delay(2*z);
      tone(GPIO_PWM_NUM, 0, z);
      delay(2*z);      
    }
  }

#endif
