#ifndef ADC_ADS1115_H
#define ADC_ADS1115_H

//Modifided by www.wizcube.eu 08/05/2022

  #define ADC_ADS1115_H
  #include "ADS1X15.h"

  void ADC_ADS1115_init();
  void ADC_ADS1115_read();
  void handleConversion();

  // choose you sensor
  // ADS1013 ADS(0x48);
  // ADS1014 ADS(0x48);
  // ADS1015 ADS(0x48);
  // ADS1113 ADS(0x48);
  // ADS1114 ADS(0x48);
  ADS1115 ADS(0x48);
  
#endif
