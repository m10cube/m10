//GREENHOUSE PROJECT 
//10/05/2020 23:35
//WHAT WE DID SO FAR
//4 X Capacitive SOIL sensors connected to ADS1115 ADC and reading values.
//MQTT client set up OK. TCP/IP Modbus will not used at the moment.
//VALVE 1 - 4 connected and working controlling the WIZcube Relay module. 
//NODE-RED and MTT connection WORKING. 
//Parametric design to get as many as sensors and valves as you like
//Diferent polling timers for valve control and MQTT messaging. Timers are using millis function
//Sound beeps for diferent tasks. LED on board used for signaling to user as well


//TO DO NEXT
//Build the NODE-RED GUI Interface to see values in widgets in a smartphone and make action control
//PID Valve Control may be next
//Temperature and moisture sensor

#include "HEADER.h"

void setup(){
  control_task_timer = control_loop_time;//Initial go to the loop
  mqtt_task_timer = mqtt_loop_time;//Initial go to the loop
  Serial.begin(115200);
  db_init();
  MQTT_init();
  LED_Flash_init();
  #if ADC_ADS1115
    ADC_ADS1115_init();
  #endif
  LED_Flash(3,50); 
  #if BUZZER_SEM
    BUZZER_init();      
     delay(500); 
     cricket(2);  // Initialisation OK sound
  #endif
}

void loop(){
    //POLL CONTROL
    if ((millis() - control_task_timer) > control_loop_time){
      ADC_ADS1115_read();
      refresh_control();
      LED_Flash(1,50);       
      #if BUZZER_SEM
         //buzzer_sound(1, 1000, 25);
      #endif
      control_task_timer = millis(); 
    }
//POLL MQTT
    if ((millis() - mqtt_task_timer) > mqtt_loop_time){
      refresh_mqtt();
      LED_Flash(2,50); 
      #if BUZZER_SEM
        //buzzer_sound(2, 1000, 25);
      #endif
      mqtt_task_timer = millis(); 
    }
}
