
void refresh_control(){
  //SEND MQTT MESSAGES
  int n; 
  while (n < channel){
    if(soil_db[n].adc < 50) {
    mqttstr = message_1 + String(n+1) + message_2 + message_3 + String(n+1) + message_4;
    mqttstr.toCharArray(msgtext_, 50);
    
    Serial.println(mqttstr);
    state_db[n+8].condition = 1; //VALVE 1 ON
    refresh_output();
    sprintf(msgtext,msgtext_,soil_db[n].adc);
    }   
    else if (soil_db[n].adc >= 50){
      mqttstr = message_1 + String(n+1) + message_6 + message_3 + String(n+1) + message_5;
      mqttstr.toCharArray(msgtext_, 50);
      Serial.println(mqttstr);
      state_db[n+8].condition = 0; //VALVE 1 OFF
      refresh_output();     
    }
    else{
        printf(msgtext,"Sensor Problem",soil_db[n].adc);
    }
    Serial.print("soil moisture: ");
    Serial.print(soil_db[n].adc);
    Serial.println("%");
    Serial.println();
    n++;
  } 
  delay(500);
  Serial.println();
  Serial.println();
}

void ETHERNET_init() {
  delay(10);
   // Init the Ethernet connection
  Ethernet.init(17);    // WIZnet W5100S-EVB-Pico SPI CS
  Ethernet.begin(mac, ip); //start the Ethernet connection and the server: 
}

void config_gpio(void){
  for(int i = 0; i < 8; i++) {
    pinMode(gpio[i], INPUT);
    pinMode(gpio[i + 8], OUTPUT);
  }       
}

void refresh_input(void){
  for(int i = 0; i < 8; i++) {
    state_db[i].condition = !digitalRead(gpio[i]);
  } 
}

void refresh_output(void){
  for(int i = 0; i < 8; i++) {
    digitalWrite(gpio[i + 8], state_db[i + 8].condition);
  }
}

void refresh_io(){
  refresh_input();        //Read input stage to record [0]     
  refresh_output();       //Write record [1] to output stage
}

  void LED_Flash_init()
  {
    pinMode(LED_BUILTIN, OUTPUT);
  }

 void LED_Flash(int flashes, int delaymS)
    //LED_Flash(50, 50);                    //50 long fast speed flash indicates LoRa device error
    //LED_Flash(25, 50);                    //25 long fast speed flash indicates WiFi device error
    //LED_Flash(2, 125);                    //2 slow speed LoRa OK
    
    {
      uint16_t index;
      for (index = 1; index <= flashes; index++){
        digitalWrite(LED1, HIGH);
        delay(delaymS);
        digitalWrite(LED1, LOW);
        delay(delaymS);
      }
    }
