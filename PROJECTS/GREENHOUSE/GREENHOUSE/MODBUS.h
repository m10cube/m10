#ifndef MODBUS_H
#define MODBUS_H
  
  /** MODBUS **/
  ModbusTCPServer modbusTCPServer;
  
  void refresh_input_modbus(void){
    uint16_t input = 0;
    for(int i = 0; i < 8; i++) {
      if(state_db[i].condition == HIGH)
        bitSet(input, i);
    }
    modbusTCPServer.holdingRegisterWrite(0x00, input);
  }
  
  void refresh_output_modbus(void){
    //Fill Output record with Modbus Output data
    for(int i = 0; i < 8; i++) {
      state_db[i+8].condition = modbusTCPServer.coilRead(i);
    }
  }
  
  
  void send_status(void){
    uint16_t stat = 0;
    for(int i = 0; i < 8; i++) {
      if(state_db[i+8].condition == HIGH)
        bitSet(stat, i);
    }
    modbusTCPServer.holdingRegisterWrite(0x01, stat);
  }
#endif
