<b>PROJECT: GREENHOUSE</b><p>
The project will be a real case study in the back of our yard.<br>
Eight tamato plants already preparing to plant and fully grow using the GREENHOUSE project.<br>
Soon can watch the project live.<p>

09/05/2020 23:55 UPDATE<br>
<b>WHAT WE DID SO FAR</b><br>
- 4 X Capacitive SOIL sensors connected to ADS1115 ADC and reading values.<br>
- VALVE 1 - 4 connected and working controlling the WIZcube Relay module.<br>
- MQTT client set up OK. TCP/IP Modbus will not used at the moment.<br>
- NODE-RED and MQTT internal broker connection WORKING.<br>
- Parametric design to get as many as sensors and valves as you like<br>
- Diferent polling timers for valve control and MQTT messaging. Timers are using millis function<br>
- Sound beeps for diferent tasks. LED on board used for signaling to user as well<p>

<b>TO DO NEXT</b><br>
- Add more 4 X Capacitive SOIL sensors.<br>
- Build the NODE-RED GUI Interface to see values in widgets in a smartphone and make action control<br>
- PID Valve Control may be next<br>
- Temperature, humidity, light, and CO2 sensor<br>
- Use camera attached to a Raspberry Pi zero. Zero  will "biggy back" on the WIZcube relay module. Similar instalation is showing on this <a href= "https://youtu.be/dg2AYa0JUbg"  target="_blank"><b>video</b></a>. Then real time steraming video or snapshots for crops growing it will find the way to Internet.<br>
Use influx to store data and Grafana to graphical view the data . All in a Raspberry Pi on top of M0CUBE PSU <a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/PSU" target="_blank"><b>M10PS01-01</b></a><p>
<div><p align="center"><b>M10RL01-20 relay module with piggy back the I2C ADC ADS1115 chip<br>
Relay Outputs switching ON/OFF following the needs for watering</b><p>
<img src="node-red soil moisture.jpg"></p></div>
<div><p align="center"><b>The Node-Red GREENHOUSE flow</b><br>
<img src="node-red design.jpg"></p></div>
<div><p align="center"><b>The Node-Red GREENHOUSE UI</b><br>
<img src="node-red ui.jpg"></p></div>
ABOUT the project:
We will start with a small test program to show you the use of the WIZcube Relay module M10RL01-20 module to water our pots.<br>
This project will continue to use more and more of these modules or the upcoming environmental sensor module to become a full Greenhouse controller.<br>
For this small project an ADC I2C module with ADS1115 chip with 4X ADC is constructed as piggy back (until the sensor board finished) on the Raspberry 40 pin connector
Only power and I2C signals needed.<br>
4 channels for sensing 4 pots and controlling the valves for watering.<br>
A POE addition is under way to power the module and switching the valve.<br>
Control will be in NODE-RED but the node can collect and store info locally and take action<br>

WIZcube modules that can be part of the CNC CONTROLLER are:<br>
<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/INPUT%20OUTPUT/M10DX01-20" target="_blank"><b>M10DX01-20</b></a> 8X24V INPUT 8X24V OUTPUT HIGH SIDE: Switching large Relay or SSR for heavy loads e.g heating water tanks that require 20 amps load.<br>
<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/OUTPUT/M10RL01-20" target="_blank"><b>M10RL01-20</b></a> 8XRELAY: Control AC or DC loads.<br>
<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/HMI" target="_blank"><b>M10HM01-20</b></a> A 24V / 5V Power Supply Unit<br>

<p>More tests or real working examples, will uploaded as soon as we finish them.</p>

<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br> 





