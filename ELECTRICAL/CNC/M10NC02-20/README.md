M10NC02-20 WIZcube version<br>
WIZnet PICO clone on board<br>
6 Axis CNC Controller. Companion modules M10DX01-20, M10HM01-20, M10RL01-20
<div><p align="center"><img src="/ELECTRICAL/IMAGES/M10NC02-20-3DTOP.png"></div></p>

<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br> 





