M10PS01-01 
Power Supply Unit
<div><p align="center"><img src="M10PS01-01-POTOP.jpg"></p></div>
Motherboard for carring a Raspberry Pi or Zero.<br>
It also includes a 2X step down 24V/5V to power a raspberry and provide power to WIZcubes attached modules via Raspberry 40 pin connector.<br>
One is used to power the Raspberry PI and the other to bring power to 40 pin connector, for more power "hungry" WIZcube applications.<br>
In most cases one can be used for both.<p>

With this type of contruction no more "power failure" sign on the screen.<br>


<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br> 





