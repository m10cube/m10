M10DX02-10 I2C expansion version<br> 
8 x Digital Opto Isolated Inputs (24V DC) plus 8 x Digital Outputs (High Side Transistor 24V DC)
<div><p align="center"><img src="/ELECTRICAL/IMAGES/M10DX02-10-3DTOP.png"></div></p>

<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br> 





