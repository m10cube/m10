M10DO01-10<br>
8 x Digital Outputs (Relay 16A)
<div><p align="center"><img src="/ELECTRICAL/IMAGES/M10DO01-10-3DTOP.png"></div></p>

<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br> 





